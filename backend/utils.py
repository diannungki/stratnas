import uuid
import bcrypt


def generate_unique_id():
    random = str(uuid.uuid4())
    random = random.upper()
    random = random.replace("-", "")
    return random[0:6]


def field_parser(data, separator):
    a_list = data.split(separator)
    return ",".join(a_list)


def gallery_serializer(gallery, many=True):
    gallery_list = []
    for g in gallery:
        media_list = []
        media_file = g.mediafile_set.all()

        for m in media_file:
            try:
                thumbnail = m.thumbnail.url
            except:
                thumbnail = None

            media_list.append({
                'media_file': m.files.url,
                'source': m.source,
                'description': m.description,
                'thumbnail': thumbnail,
            })

        gallery_list.append({
            'title': g.title,
            'slug': g.slug,
            'date': g.date,
            'category': g.category,
            'media': media_list,
        })

    if not many:
        try:
            return gallery_list[0]
        except:
            return {}

    return gallery_list


def generate_token(token):
    passwd = bytes(token, 'utf-8')
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(passwd, salt)
    return hashed.decode("utf-8")


def match_token(hashed, matcher):
    hashed = bytes(hashed, 'utf-8')
    matcher = bytes(matcher, 'utf-8')
    if bcrypt.checkpw(matcher, hashed):
        return True
    return False
