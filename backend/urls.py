from django.urls import path
from backend import views

urlpatterns = [
    path('gallery/', views.gallery),
    path('gallery/<slug:slug>/', views.gallery_slug),
    path('ebook/', views.ebook),
    path('ebook/<code>/', views.ebook_download),
    path('institution/', views.institution),
    path('institution/confirm/', views.confirm_institution),
    path('institution/update/', views.update_institution),
    path('institution/export/', views.export_institution),
    path('comments/', views.comment),
    path('comments/inline/', views.comment_inline),
]
